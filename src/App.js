import React from "react";
import { useState } from 'react';
import "./App.css";

import dice_1 from "./assets/1.png";
import dice_2 from "./assets/2.png";
import dice_3 from "./assets/3.png";
import dice_4 from "./assets/4.png";
import dice_5 from "./assets/5.png";
import dice_6 from "./assets/6.png";


function App() {
  var diceImages = [
    dice_1,
    dice_2,
    dice_3,
    dice_4,
    dice_5,
    dice_6 
  ]

  const [image, setNewImage] = useState(diceImages[0])
  const [image2, setNewImage2] = useState(diceImages[1])
  const rollDice = () => {
    //Generate random number
    var randomNumber = Math.floor(Math.random() * 6);
    var randomNumber2 = Math.floor(Math.random() * 6);
    setNewImage(diceImages[randomNumber]);
    setNewImage2(diceImages[randomNumber2]);
  }


  return (
    <div>
      <center>
        <h1 style={{marginTop: "25px"}}>Lucky Dice Casino</h1>
        <div className="container">
          <img className="square" src={image}></img>
          <div style={{width:"5px", display:"inline-block"}}></div>
          <img className="square" src={image2}></img>
        </div>
        <button type="button" class="btn btn-warning" onClick={rollDice}>Roll</button>
      </center>
    </div>
  );
}

export default App;
